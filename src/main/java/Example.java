import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;

import java.util.List;
import java.util.function.Function;

/**
 * Created by islam on 3/28/16.
 */
public class Example {
    public static void main(String[] args) {
        //Boilerplate needed in every application

        SparkConf config = new SparkConf() //This is the configuration for the application, if running on a cluster we would do this in a config file
                .setMaster("local[4]") //sets the master node and number of cores to use
                .setAppName("BDProject"); //Every spark application must have a name

        JavaSparkContext sc = new JavaSparkContext(config);
        SQLContext sqlContext = new SQLContext(sc);

        //Reading tables that we have, each table is really a directory
        DataFrame tables = sqlContext.read().parquet("ParquetTables/menu");
        //Prints a small subset of the data
        tables.show();
        //Print the schema so we know what datatypes we are working with
        tables.printSchema();


        //We have a sql api if we want to do transformations through sql
        tables.registerTempTable("menus");

        //Java 8's Functional api is useful in this sense becuase we can define custom funcitons to apply to data
        //Here is a silly example of multiplying each price by 5
        Function<Double, Double> multiplyBy5 = x -> x*5;
        sqlContext.udf().register("multiplyBy5", (Double x) -> multiplyBy5.apply(x), DataTypes.DoubleType);


        sqlContext.sql("Select *, multiplyBy5(Price) from menu");

        //Alternativly sparks main data abstaction is called an RDD this is basically a distributed array and we can work
        //with an rdd instead

        JavaRDD<Row> rdd = tables.toJavaRDD();

        //Here you can pass in functions to map or reduce the data as well as a lot more see the RDD api docs
        String concat = rdd.
            map(r -> r.getString(0)) //get first element in each string
            .reduce((s1, s2) -> s1.concat(s2)); //Concatenate all first elements

        //If the data all fits on a single machines memory you don't have to work with it as an rdd or dataframe
        //It can all be collected into an array. And then on each row just use the get function.
        List<Row> rowList = rdd.collect();
        System.out.println(rowList.get(0).getString(0));

        //This is just intended as an introduction and to get you started. The docs can be found at
        // http://spark.apache.org/docs/latest/sql-programming-guide.html#udf-registration-moved-to-sqlcontextudf-java--scala

        //The machine learning library is called MLLib if you want to look at it as well
        //http://spark.apache.org/docs/latest/mllib-guide.html
    }
}
