import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.{Row, DataFrame, SQLContext}
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by islam on 3/29/16.
 */
object ScalaExample {

  def main(args: Array[String]) {
    val config = new SparkConf().setMaster("local[4]").setAppName("BDProject")
    val sc = new SparkContext(config)
    val sqlContext = new SQLContext(sc)
    //Reading tables that we have, each table is really a directory
    val tables: DataFrame = sqlContext.read.parquet("ParquetTables/menu")
    //Prints a small subset of the data
    tables.show()
    //Print the schema so we know what datatypes we are working with
    tables.printSchema()


    //We have a sql api if we want to do transformations through sql
    tables.registerTempTable("menus")
    //We can define custom funcitons to apply to data
    //Here is a silly example of multiplying each price by 5
    sqlContext.udf.register("multiplyBy5", (x:Double) => x * 5)

    sqlContext.sql("Select *, multiplyBy5(Price) from menu")
    //Alternatively sparks main data abstraction is called an RDD this is basically a distributed array and we can work
    //with an rdd instead
    val rdd = tables.rdd
    //Here you can pass in functions to map or reduce the data as well as a lot more see the RDD api docs
    val concat: String = rdd.map(r => r.getString(0)).reduce((s1, s2) => s1.concat(s2))
    //If the data all fits on a single machines memory you don't have to work with it as an rdd or dataframe
    //It can all be collected into an array. And then on each row just use the get function.
    val rowList: Array[Row] = rdd.collect()
    println(rowList(0).getString(0))

    //This is just intended as an introduction and to get you started. The docs can be found at
    // http://spark.apache.org/docs/latest/sql-programming-guide.html#udf-registration-moved-to-sqlcontextudf-java--scala
    //The machine learning library is called MLLib if you want to look at it as well
    //http://spark.apache.org/docs/latest/mllib-guide.html
  }

}
